#!/usr/bin/python
"""
	aac_smart_
	Gets graphs for all disks for a given smart attribute

	Copy or symlink this executable with the end being the attribute
	id of which to fetch
"""
from sys import argv, exit
from subprocess import check_output
from re import search, sub
from xml.etree import ElementTree

smart_stats = {}

rex = search("aac_smart_([^$]+)$", argv[0])
if not rex:
	print("Attribute must be specified at end of file name")
	print("Eg: ")
	print("   aac_smart_   -> aac_smart_C2  (for drive temperature)")
	exit(2)
get_attribute = int(rex.group(1), 16)
output = check_output(["arcconf", "GETSMARTSTATS", "1"])

# First parse and sanity check
sata_section_begin = output.index("<SmartStats")
sata_section_end = output.index("</SmartStats>") 
if not sata_section_begin or not sata_section_end:
	print("No valid SATA sections in SMART stats output")
	exit(3)

smart_section = output[sata_section_begin:sata_section_end+13]
root = ElementTree.fromstring(smart_section)
physical_drive_stats = root.findall("PhysicalDriveSmartStats")

if not physical_drive_stats or not len(physical_drive_stats):
	print("No drives")
	exit(4)


for drive in physical_drive_stats:
	drive_stats = {}
	drive_id = drive.attrib["id"]
	for attribute in drive.findall("Attribute"):
		stats = {}
		attribute_id = attribute.attrib["id"]
		attribute_raw = attribute.attrib["rawValue"]
		attribute_current = attribute.attrib["normalizedCurrent"]
		attribute_worst = attribute.attrib["normalizedWorst"]
		attribute_name = attribute.attrib["name"]
		stats["id"] = attribute_id
		stats["name"] = attribute_name
		stats["raw"] = attribute_raw
		stats["worst"] = attribute_worst
		stats["current"] = attribute_current
		drive_stats[int(attribute_id, 16)] = stats
		title = attribute_name
	smart_stats[drive_id] = drive_stats
		
title = ""
config = ""
for disk in smart_stats:
	if get_attribute in smart_stats[disk]:
		title = smart_stats[disk][get_attribute]["name"]
		config += """aac_smarts_{0}_{1}_c.label {2} Current		
aac_smarts_{0}_{1}_c.draw LINE1
aac_smarts_{0}_{1}_w.label {2} Worst
aac_smarts_{0}_{1}_w.draw LINE1
""".format(disk, get_attribute, smart_stats[disk][get_attribute]["name"])

if len(argv) > 1 and argv[1] == "config":
        print("""graph_args -l 0
graph_vlabel value
graph_title Adaptec SMART - {}
graph_category adaptec
""".format(title) + config)
else:
	for disk in smart_stats:
		if get_attribute in smart_stats[disk]:
			print("aac_smarts_{0}_{1}_c.value {2}".format(disk, get_attribute, smart_stats[disk][get_attribute]["current"]))
			print("aac_smarts_{0}_{1}_w.value {2}".format(disk, get_attribute, smart_stats[disk][get_attribute]["worst"]))
	

exit(0)
